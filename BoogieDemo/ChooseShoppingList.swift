

import UIKit
import DropDown
import SVProgressHUD

class SavedList {
    var listid: String?
    var listname: String?
    var currentdate: String?
   
    init(dict: [String: Any]){
        self.listid = dict["id"] as? String
        self.listname = dict["list_name"] as? String
        self.currentdate = dict["created_on"] as? String
    }
}
class ItemList {
    var itemname: String?
    var itemqty: String?
    var itembrand: String?
    
    init(dict: [String: Any]){
        self.itemname = dict["item_name"] as? String
        self.itemqty = dict["item_qty"] as? String
        self.itembrand = dict["item_brand"] as? String
    }
}


class ChooseShoppingList: UIViewController,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet weak var lblEditListName: UILabel!
    @IBOutlet weak var tblCreateOrder: UITableView!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var imgStore: UIImageView!
    @IBOutlet weak var btnChooseList: UIButton!
    
   
    @IBOutlet weak var vwPopup: UIView!
    @IBOutlet weak var vwAlert: UIView!
    @IBOutlet weak var tfEditListName: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    var storeId =  String()
    var nameStore = String()
    var logoUrl = String()
    var arr = [""]
    var savedListInfo = [SavedList]()
    let dropdownChooseList = DropDown()
    var itemsArr = [ItemList]()
    var listid = String()
    var listname = String()
    let userid = UserDefaults.standard.string(forKey: "userid")!
    override func viewDidLoad() {
        super.viewDidLoad()
        itemsArr.append(ItemList(dict: NSDictionary() as! [String : Any]))
        vwPopup.isHidden = true
        tfEditListName.delegate = self
        
        tblCreateOrder.delegate = self
        tblCreateOrder.dataSource = self
        
        vwAlert.layer.cornerRadius = 8
        btnSave.layer.borderWidth = 1
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor.lightGray.cgColor
        
        let username = UserDefaults.standard.string(forKey: "username")!
        lblWelcome.text = "Welcome \(username) to \(nameStore)"
        let image = URL(string: logoUrl)
        imgStore.sd_setImage(with: image!)
        }
 
    override func viewWillAppear(_ animated: Bool) {
        
        HitApi.share().getListNames(user_id: self.userid){ ( Message, Status, savedlist) in
            
            if savedlist.count > 0 {
                self.savedListInfo = savedlist
                self.dropDownSubData(subName: self.savedListInfo.map{$0.listname!})
            }else{
                ToastView.shared.short(self.view, txt_msg: "List not available")
            }
        }
        
    }
    
    
   // Dropdown
    func dropDownSubData(subName:[String]){
        dropdownChooseList.anchorView = btnChooseList
        dropdownChooseList.dataSource = subName
        dropdownChooseList.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnChooseList.setTitle(item, for: .normal)
            self.listname = item
            self.listid = self.savedListInfo[index].listid!
            HitApi.share().getitemsFromsavedlist(list_id: self.listid){(message, status, getItem) in
                
                if getItem.count > 0 {
                    self.itemsArr = getItem
                    self.tblCreateOrder.reloadData()
                    
                }else{
                  ToastView.shared.short(self.view, txt_msg: message)
                }
            }
        }
    }
    
    
    //api hit for the choosing list
    @IBAction func btnChooseListHit(_ sender: Any) {
        if self.savedListInfo.count <= 0 {
            ToastView.shared.short(self.view, txt_msg: "No List Available")
        }
        else{
            dropdownChooseList.show()
            
        }
    }
    
    
    
    
    
    //save list button
    @IBAction func btnSaveListHit(_ sender: Any) {
        if listname.isEmpty
        {
            lblEditListName.text = "Enter List Name"
        }
        else{
            lblEditListName.text = "Edit List Name"
            tfEditListName.text = self.listname
        }
        self.vwPopup.isHidden = false
    }
    
    
    
    
    //function JSONStringfy defined here
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                 print("error")
                }
        }
        return ""
    }
    
    
    
    
    
    
    
    
    
    
    //naviagtions
    @IBAction func btnHomePressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "TabBarClass") as! TabBarClass
        let subdata = data.viewControllers![0] as! ChooseServiceTabVC
        let loc_id = UserDefaults.standard.string(forKey: "locid")!
        let loc_name = UserDefaults.standard.string(forKey: "locname")!
        subdata.locId = loc_id
        subdata.locName = loc_name
        
        navigationController?.pushViewController(data, animated: true)
    }
    
  //back button
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    //table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseShoppingCell", for: indexPath) as! ChooseShoppingCell
        cell.tfItem.text = itemsArr[indexPath.row].itemname ?? ""
        cell.tfQty.text = itemsArr[indexPath.row].itemqty ?? ""
        cell.tfBrand.text = itemsArr[indexPath.row].itembrand ?? ""
        
        cell.btnAddRow.tag = indexPath.row
        cell.btnDeleteRow.tag = indexPath.row
        cell.tfBrand.tag = indexPath.row
        cell.tfQty.tag = indexPath.row
        cell.tfItem.tag = indexPath.row
        cell.tfItem.addTarget(self, action: #selector(tfEditName(_:)), for: .editingChanged)
        cell.tfBrand.addTarget(self, action: #selector(tfEditBrand(_:)), for: .editingChanged)
        cell.tfQty.addTarget(self, action: #selector(tfEditQty(_:)), for: .editingChanged)
        
        cell.btnAddRow.addTarget(self, action: #selector(addRow), for: .touchUpInside)
        cell.btnDeleteRow.addTarget(self, action: #selector(deleteRow), for: .touchUpInside)
        
        return cell
    }
    @objc func tfEditName(_ sender: UITextField){
        itemsArr[sender.tag].itemname = sender.text
    }
    @objc func tfEditBrand(_ sender: UITextField){
        itemsArr[sender.tag].itembrand = sender.text
    }
    @objc func tfEditQty(_ sender: UITextField){
        itemsArr[sender.tag].itemqty = sender.text
    }
    
    @objc func addRow(sender: UIButton){
        let index = sender.tag
        itemsArr.append(ItemList(dict: NSDictionary() as! [String : Any]))
        tblCreateOrder.insertRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        tblCreateOrder.reloadData()
    }
    
    @objc func deleteRow(sender: UIButton){
        if itemsArr.count > 1{
            let index = sender.tag
            itemsArr.remove(at: index)
            tblCreateOrder.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
            tblCreateOrder.reloadData()
    }
    
    }
    
    // review list
    
    @IBAction func btnReviewList(_ sender: Any) {
        let count = itemsArr.filter{$0.itemqty == nil || $0.itemname == nil}.count
        
        if count > 0
        {
            ToastView.shared.short(self.view, txt_msg: "Feilds Can't be Empty")
            return
        }
        else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let data = storyboard.instantiateViewController(withIdentifier: "ReviewItemVC") as! ReviewItemVC
            
            data.itemList = itemsArr
            data.storeid = self.storeId
            data.storename = self.nameStore
            data.storelogo = self.logoUrl
            navigationController?.pushViewController(data, animated: true)
        }
    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        if (tfEditListName.text?.isEmpty)!{
            ToastView.shared.short(self.view, txt_msg: "List name can't be empty")
            return
        }
        else{
            self.listname = tfEditListName.text!
            
            self.btnChooseList.setTitle(listname, for: .normal)
            vwPopup.isHidden = true
            
            let count = itemsArr.filter{$0.itemqty == nil || $0.itemname == nil}.count
            
            if count > 0
            {
                ToastView.shared.short(self.view, txt_msg: "Feilds Can't be Empty")
                return
            }
            else{
                var itemArr:[Any] = []
                for item in itemsArr{
                    let itemDict:[String:Any] = ["item_name":item.itemname ?? "",
                                                 "item_qty":item.itemqty ?? "",
                                                 "item_brand":item.itembrand ?? ""]
                    itemArr.append(itemDict)
                }
                
                HitApi.share().savelist(listid: self.listid, userid: self.userid, itemArray: JSONStringify(value: itemArr as AnyObject) as AnyObject, listname: self.listname){ (message, status) in
                   ToastView.shared.short(self.view, txt_msg: message)
                    self.viewWillAppear(true)
                }
          }
        }
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        vwPopup.isHidden = true
    }
}
