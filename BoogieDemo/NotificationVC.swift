

import UIKit
import SVProgressHUD

class Notify{
    
    var notiId: String?
    var notiMsg: String?
    var createdon: String?
    
    init(dict: [String:Any]){
     self.notiId = dict["id"] as? String
     self.notiMsg = dict["notification_message"] as? String
     self.createdon = dict["created_on"] as? String
    }
}

class NotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
   @IBOutlet weak var tblNotifications: UITableView!
    
    var getNotif = [Notify]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblNotifications.delegate = self
        tblNotifications.dataSource =  self
        getNotifications()
        
    }
    
    func getNotifications(){
        let userid = UserDefaults.standard.string(forKey: "userid")!
        HitApi.share().getNotifications(userid: userid) { (message, status, response) in
            if status{
                self.getNotif = response
                DispatchQueue.main.async {
                    self.tblNotifications.reloadData()
                    ToastView.shared.short(self.view, txt_msg: message)
                }
            }
            else{
                ToastView.shared.short(self.view, txt_msg: message)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getNotif.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.lblDateTime.text = getNotif[indexPath.row].createdon
        cell.lblNotificationMsg.text = getNotif[indexPath.row].notiMsg
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
