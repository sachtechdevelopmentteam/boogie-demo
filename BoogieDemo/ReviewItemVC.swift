
import UIKit

class ReviewItemVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
   @IBOutlet weak var tblReviewItem: UITableView!
   @IBOutlet weak var btnCheckBox: UIButton!
    
    var storename = String()
    var storelogo = String()
    var storeid = String()
    var itemList = [ItemList]()
    var ischecked: Bool!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReviewItem.delegate = self
        tblReviewItem.dataSource = self
        self.ischecked = false
        btnCheckBox.setImage(UIImage(named:"check-box-empty"), for: .normal)
        btnCheckBox.setImage(UIImage(named:"checked"), for: .selected)
      
    }
    

    @IBAction func btnEditList(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCompleteOrder(_ sender: Any) {
        if self.ischecked {
        var itemArrList:[Any] = []
        for item in self.itemList{
            let itemDict:[String:Any] = ["item_name":item.itemname ?? "",
                                         "item_qty":item.itemqty ?? "",
                                         "item_brand":item.itembrand ?? ""]
            itemArrList.append(itemDict)
        }
        
        HitApi.share().createorder(itemArray: self.JSONStringify(value: itemArrList as AnyObject) as AnyObject, storeId: self.storeid, customerId: UserDefaults.standard.string(forKey: "userid")!){ (message, status) in
            if status{
               ToastView.shared.short(self.view, txt_msg: message)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let data = storyboard.instantiateViewController(withIdentifier: "LastScreenVC") as! LastScreenVC
                data.storename = self.storename
                data.storeimage = self.storelogo
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.navigationController?.pushViewController(data, animated: true)
                })
            }else{
            
                ToastView.shared.short(self.view, txt_msg: message)
             }
            }
            
        }else{
            ToastView.shared.short(self.view, txt_msg: "Need to agree to Terms And Conditions")
        }
    }
    
    //function JSONStringfy defined here
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
            
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
            
            if JSONSerialization.isValidJSONObject(value) {
                
                do{
                    let data = try JSONSerialization.data(withJSONObject: value, options: options)
                    if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                        return string as String
                    }
                }catch {
                    print("error")
                }
            }
            return ""
       
        }
  
    
    @IBAction func btnChecked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
        }) { (success) in
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
              
                sender.isSelected = !sender.isSelected
                sender.transform = .identity
                
            }, completion: nil)
        }
        let check = sender.isSelected
        self.ischecked = !check
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewItemCell", for: indexPath) as! ReviewItemCell
        cell.lbltem.text = itemList[indexPath.row].itemname
        cell.lblQty.text = itemList[indexPath.row].itemqty
        cell.lblBrand.text = itemList[indexPath.row].itembrand
        return cell
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
