

import UIKit

class AvailableStoreCell: UITableViewCell {

    @IBOutlet weak var imgStoreLogo: UIImageView!
    
    @IBOutlet weak var lblStoreStatus: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
