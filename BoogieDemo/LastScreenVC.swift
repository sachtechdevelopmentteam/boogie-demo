


import UIKit

class LastScreenVC: UIViewController {

    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var lblUser: UILabel!
    
  
   
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var imgStoreLogo: UIImageView!
    @IBOutlet weak var vwRateView: UIView!
    @IBOutlet weak var popView: UIView!
    
    var storename = String()
    var storeimage = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        let username = UserDefaults.standard.string(forKey: "username")!
        self.lblUser.text = "Thank You, \(username)"
        let image = URL(string: storeimage)
        imgStoreLogo.sd_setImage(with: image!)
        self.lblOrderStatus.text = "Your order at \(self.storename) placed successfully."
        addshodow(component: vwRateView)
     
    }
    

    @IBAction func btnDropReview(_ sender: Any) {
        
       self.popView.isHidden = false
        
    }
    
    
    
    @IBAction func btnRemindTapped(_ sender: Any) {
        self.popView.isHidden = true
    }
  
    @IBAction func btnGoHome(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "TabBarClass") as! TabBarClass
        let subdata = data.viewControllers![0] as! ChooseServiceTabVC
        let loc_id = UserDefaults.standard.string(forKey: "locid")!
        let loc_name = UserDefaults.standard.string(forKey: "locname")!
        subdata.locId = loc_id
        subdata.locName = loc_name
        self.navigationController?.pushViewController(data, animated: true)
    }
   
    func addshodow(component:UIView)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
        component.layer.borderWidth = 1
        component.layer.borderColor = UIColor.white.cgColor
        component.layer.cornerRadius = 10
    }
}
