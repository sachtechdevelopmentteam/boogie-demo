

import UIKit
import WebKit


class TermsVC: UIViewController{

    @IBOutlet weak var vwHeading: UIView!
    
    @IBOutlet weak var termsView: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        let webView = WKWebView(frame: .zero)
        termsView.addSubview(webView)
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        let height = NSLayoutConstraint(item: webView, attribute: .height, relatedBy: .equal, toItem: termsView, attribute: .height, multiplier: 1, constant: 0)
        let width = NSLayoutConstraint(item: webView, attribute: .width, relatedBy: .equal, toItem: termsView, attribute: .width, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: webView, attribute: .leftMargin, relatedBy: .equal, toItem: termsView, attribute: .leftMargin, multiplier: 1, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: webView, attribute: .rightMargin, relatedBy: .equal, toItem: termsView, attribute: .rightMargin, multiplier: 1, constant: 0)
        let bottomContraint = NSLayoutConstraint(item: webView, attribute: .bottomMargin, relatedBy: .equal, toItem: termsView, attribute: .bottomMargin, multiplier: 1, constant: 0)
        termsView.addConstraints([height, width, leftConstraint, rightConstraint, bottomContraint])
        
        let myURL = URL(string: "https://app.termly.io/document/terms-and-conditions/86949fd3-52b0-4931-88ec-5f79b9a9ce6b")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
