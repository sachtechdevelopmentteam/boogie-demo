

import UIKit
class Stores {
    var storeid: String?
    var ownerId: String?
    var categoryId: String?
    var storeName: String?
    var storeImg: String?
    var storeImgPath: String?
    var storeShutter: String?
    
    init(dict: [String: Any]){
        self.storeid = dict["id"] as? String
        self.ownerId = dict["store_owner_id"] as? String
        self.categoryId = dict["category_id"] as? String
        self.storeName = dict["store_name"] as? String
        self.storeImg = dict["store_img"] as? String
        self.storeImgPath = dict["store_img_path"] as? String
        self.storeShutter = dict["store_shutter"] as? String
    }
}

class AvailableStoreVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

   
    @IBOutlet weak var tblAvailableStore: UITableView!
    
    @IBOutlet weak var lblCategoryName: UILabel!
    var category_name = String()
    var category_id = String()
    let BASE_URL = "http://stsmentor.com/boogie"
    var storesAvaialble = [Stores]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAvailableStore.delegate = self
        tblAvailableStore.dataSource = self
    
        lblCategoryName.text = "Category: \(category_name)"
        getStoreData()
    }
    func getStoreData() {
        
        HitApi.share().availableStore(categoryId: category_id ) { (message, status, response) in
            
            if response.count > 0 {
                self.storesAvaialble = response
                DispatchQueue.main.async {
                    self.tblAvailableStore.reloadData()
                }
                
            } else {
                let alertController = UIAlertController(title: "Error", message: "No Stores Available!", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storesAvaialble.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableStoreCell", for: indexPath) as! AvailableStoreCell
        cell.lblStoreName.text = storesAvaialble[indexPath.item].storeName
        cell.lblStoreStatus.text = storesAvaialble[indexPath.item].storeShutter
        let status = "open"
        if cell.lblStoreStatus.text == status {
            cell.lblStoreStatus.textColor = UIColor.green
        }
        else{
            cell.lblStoreStatus.textColor = UIColor.red
        }
        let imgPath = storesAvaialble[indexPath.item].storeImgPath
        let imgName = storesAvaialble[indexPath.item].storeImg
        let imgUrl = "\(BASE_URL)\(imgPath!)\(imgName!)"
        let image = URL(string: imgUrl)
        cell.imgStoreLogo.sd_setImage(with: image!)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "ChooseShoppingList") as! ChooseShoppingList
        data.nameStore = storesAvaialble[indexPath.item].storeName!
       let imgPath = storesAvaialble[indexPath.item].storeImgPath!
        let imgName = storesAvaialble[indexPath.item].storeImg!
        
        let imgUrl = "\(BASE_URL)\(imgPath)\(imgName)"
        data.logoUrl = imgUrl
        data.storeId = storesAvaialble[indexPath.item].storeid!
        navigationController?.pushViewController(data, animated: true)
    }
   
   
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
