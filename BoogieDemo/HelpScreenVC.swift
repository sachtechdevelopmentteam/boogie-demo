

import UIKit

class HelpScreenVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var btnSkipScreens: UIButton!
    @IBOutlet weak var btnNextScreen: UIButton!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
      navigationController?.navigationBar.isHidden = true
        self.scrollView.delegate = self
        btnNextScreen.addTarget(self, action: #selector(btnNextScreenClicked), for: .touchUpInside)
    
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        if (scrollView.contentOffset.x == self.view.bounds.width * 3){
            self.btnNextScreen.isHidden = true
          
        }
    }
 
   
    
    @objc func btnNextScreenClicked() {
        if(scrollView.contentOffset.x < self.view.bounds.width * 3){
            
            scrollView.contentOffset.x +=  self.view.bounds.width
            if (scrollView.contentOffset.x == self.view.bounds.width * 3){
                self.btnNextScreen.isHidden = true
                let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                pageControl.currentPage = Int(pageNumber)
            }else{
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
        }
    }

}

    @IBAction func btnSkipHit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(data, animated: true)
    }
}


