

import UIKit
class Redeem{
    
    var earnedpoints: String?
    var completeorderpoints: String?
    var redeemedpoints: String?
    var totalorder: String?
    
    init(dict: [String: Any]){
        self.earnedpoints = dict["order_reward_point"] as? String
        self.completeorderpoints = dict["total_used_points"] as? String
        self.redeemedpoints = dict["user_redeemd_point"] as? String
        self.totalorder = dict["total_order"] as? String
    }
}

class RedeemVC: UIViewController {

    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblCompletedOrder: UILabel!
    
    @IBOutlet weak var lblRedeemPoints: UILabel!
    @IBOutlet weak var lblEarnedPoints: UILabel!
    
    var totalorder = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        let userid = UserDefaults.standard.string(forKey: "userid")!
        let username = UserDefaults.standard.string(forKey: "username")
        lblWelcome.text = username
        HitApi.share().redeemPoint(userid: userid) { (earnedpoints, completeorderpoints, redeemedpoints, totalorder) in
            self.lblCompletedOrder.text = completeorderpoints
            self.lblRedeemPoints.text = redeemedpoints
            self.lblEarnedPoints.text = earnedpoints
            self.totalorder = totalorder
            
        
        }

    }
    
    @IBAction func btnRedeemTapped(_ sender: Any) {
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
