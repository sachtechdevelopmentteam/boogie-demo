

import UIKit
import SDWebImage

class Categories {
    var id: String?
    var locationId: String?
    var categoryName: String?
    var categoryImg: String?
    var categoryImgPath: String?
    var categoryStatus: String?
    
    init(dict: [String: Any]){
        self.id = dict["id"] as? String
        self.locationId = dict["location_id"] as? String
        self.categoryName = dict["category_name"] as? String
        self.categoryImg = dict["category_img"] as? String
        self.categoryImgPath = dict["category_img_path"] as? String
        self.categoryStatus = dict["category_status"] as? String
    }
}

class ChooseServiceTabVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var serviceSelection: UICollectionView!
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblLocationName: UILabel!
    var locId: String = ""
    var locName: String = ""
    var service = [Categories]()
    let BASE_URL = "http://stsmentor.com/boogie"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serviceSelection.delegate = self
        self.serviceSelection.dataSource = self
        self.lblLocationName.text = "Location: \(locName)"
        let username = UserDefaults.standard.string(forKey: "username")!
        self.lblWelcome.text = "Welcome \(username)"
        getCategories()
       
       
    }
    
 
    func getCategories(){
        HitApi.share().chooseCategory(locationId: locId ) { (message, status, response) in
            
            if response.count > 0 {
                self.service = response
                DispatchQueue.main.async {
                    self.serviceSelection.reloadData()
                }
            
            } else {
                let alertController = UIAlertController(title: "Error", message: "No Services Available!", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.service.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
            cell.category_id = service[indexPath.item].id!
            cell.lblServiceName.text = service[indexPath.item].categoryName
            let imgPath = service[indexPath.item].categoryImgPath
            let imgName = service[indexPath.item].categoryImg
            let imgUrl = "\(BASE_URL)\(imgPath!)\(imgName!)"
            let image = URL(string: imgUrl)
            cell.imgService.sd_setImage(with: image!)
            return cell
        }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "AvailableStoreVC") as! AvailableStoreVC
         data.category_name = service[indexPath.item].categoryName!
        data.category_id = service[indexPath.item].id!
        navigationController?.pushViewController(data, animated: true)
    }
}
    

  


