

import UIKit
import SVProgressHUD

class FeedbackVC: UIViewController {

    @IBOutlet weak var twFeedbackText: UITextView!
     let user_id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    

   
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSendFeed(_ sender: Any) {
      SVProgressHUD.show()
        let user_id = UserDefaults.standard.string(forKey: "userid")!
        HitApi.share().sendFeedback(feedback: twFeedbackText.text!, userid: user_id) { (message, status) in
            if status{
                SVProgressHUD.dismiss()
                ToastView.shared.short(self.view, txt_msg: message)
                print(message)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.dismiss(animated: true, completion: nil)
                    self.twFeedbackText.text! = ""
                })
            } else {
                ToastView.shared.short(self.view, txt_msg: message)
                print(message)
            }
        }
    }
}
