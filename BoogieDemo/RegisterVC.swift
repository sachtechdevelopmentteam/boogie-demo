

import UIKit
import SVProgressHUD
struct Register {
    
    var id: String?
    var username: String?
    var useremail: String?
    var logintype:String?
    var fcmtoken: String?
    var deviceid: String?
    var usermobile: String?
    var useraddress: String?
    var currentdate: String?
    
    
     init(dict: [String: Any]){
        self.id = dict["id"] as? String
        self.username = dict["username"] as? String
        self.useremail = dict["useremail"] as? String
        self.logintype = dict["login_type"] as? String
        self.fcmtoken = dict["fcm_token"] as? String
        self.deviceid = dict["device_id"] as? String
        self.usermobile = dict["user_mobile"] as? String
        self.useraddress = dict["user_address"] as? String
        self.currentdate = dict["created_on"] as? String
    }
}


class RegisterVC: UIViewController {

    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
   
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        leftImageTextfield(image: "user-7", textfield: tfName)
        leftImageTextfield(image: "close-envelope-2", textfield: tfEmail)
        leftImageTextfield(image: "lock-3", textfield: tfPassword)
        leftImageTextfield(image: "smartphone-call-2", textfield: tfMobile)
        leftImageTextfield(image: "maps-and-flags-2", textfield: tfAddress)
        addshodow(component: btnSignup)
       }
    
    
    func addshodow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
        component.layer.borderWidth = 1
        component.layer.borderColor = UIColor.white.cgColor
        component.layer.cornerRadius = 10
    }
    
    @IBAction func btnSignupTapped(_ sender: Any) {
        if self.tfName.text!.isEmpty || self.tfEmail.text!.isEmpty || self.tfMobile.text!.isEmpty || self.tfPassword.text!.isEmpty || self.tfAddress.text!.isEmpty {
            
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter  required data", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else if !isValidEmail(self.tfEmail.text!) {
            let alertController = UIAlertController(title: "Invalid Format", message: "Please enter valid username", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            SVProgressHUD.show()
            HitApi.share().signupUser(username: self.tfName.text!, usertype: "customer", userpass: self.tfPassword.text!, useremail: self.tfEmail.text!, usermobile: self.tfMobile.text!, useradd: self.tfAddress.text!) { (message, status, regdata) in
                if status{
                    print(message)
                    let user_id = regdata.id!
                    let user_name = regdata.username
                    let user_email = regdata.useremail
                    let login_type = regdata.logintype!
                    let fcm_token = regdata.fcmtoken!
                    let device_id = regdata.deviceid!
                    let user_mobile = regdata.usermobile
                    let user_add = regdata.useraddress
                    let current_date = regdata.currentdate!
                    
                    UserDefaults.standard.set(user_id, forKey: "userid")
                    UserDefaults.standard.set(user_name, forKey: "username")
                    UserDefaults.standard.set(user_email, forKey: "email")
                    UserDefaults.standard.set(login_type, forKey: "logintype")
                    UserDefaults.standard.set(fcm_token, forKey: "fcmtoken")
                    UserDefaults.standard.set(device_id, forKey: "deviceid")
                    UserDefaults.standard.set(user_mobile, forKey: "mobile")
                    UserDefaults.standard.set(user_add, forKey: "address")
                    UserDefaults.standard.set(current_date, forKey: "currentdate")
                 
                    SVProgressHUD.dismiss()
                    
                    ToastView.shared.short(self.view, txt_msg: message)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let data = storyboard.instantiateViewController(withIdentifier: "ChooseLocation") as! ChooseLocation
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        self.navigationController?.pushViewController(data, animated: true)
                    })
                    
                    self.tfEmail.text! = ""
                    self.tfPassword.text! = ""
                    self.tfName.text! = ""
                    self.tfMobile.text! = ""
                    self.tfAddress.text! = ""
                    
                }
                else{
                    SVProgressHUD.dismiss()
                    let alertController = UIAlertController(title: "Invalid Sign Up", message: message, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
        }
    
    }
 
    

    @IBAction func btnAlreadyUser(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func leftImageTextfield(image: String, textfield: UITextField)  {
        let imageView = UIImageView(image: UIImage(named: image));
        imageView.frame = CGRect(x: 8, y: 4, width: 24, height: 24);
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
    }
    

    func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
