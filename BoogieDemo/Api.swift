
import Foundation
import FirebaseInstanceID
import UIKit

class HitApi
{
    public static let BASE_URL = "http://stsmentor.com/boogie/api"
   
    
    
    private static var api:HitApi!
    private static var apiLinker:ApiLinker!
    var dataArr = [String:AnyObject]()
    let defaults:UserDefaults = UserDefaults.standard
    
    let API_KEY = "boogie@123_*"
    
    
    
    class func share() -> HitApi{
        if api == nil {
            api = HitApi()
            apiLinker = ApiLinker()
        }
        return api
    }
    

    let LOGIN_BASE_URL = BASE_URL+"/User/signin"
    let SIGNUP_BASE_URL = BASE_URL+"/User/signup"
    let LOCATION_URL = BASE_URL+"/Location/location_list"
    let STORELIST = BASE_URL+"/Store/store_list"
    let RESETPASSWORD = BASE_URL+"/User/resetPassword"
    let FORGOTPASSWORD = BASE_URL+"/User/forgetPassword"
    let ORDERCREATE = BASE_URL+"/Items/orderItem"
    let ORDERLIST = BASE_URL+"/Items/order_list"
    let UPDATEUSER = BASE_URL+"/User/update"
    let SENDFEEDBACK = BASE_URL+"/Feedback/feedbackSave"
    let FETCHORDERLIST = BASE_URL+"/Items/order_list"
    let SAVELIST = BASE_URL+"/User/addList"
    let GETLISTNAME = BASE_URL+"/User/listName"
    let GETITEMSFROMLIST = BASE_URL+"/User/itemList"
    let DELETELIST = BASE_URL+"/User/deleteList"
    let CANCELORDER = BASE_URL+"/Items/cancelOrder"
    let GETCATEGORIES = BASE_URL+"/Category/categoryList"
    let REDEEMPOINTS = BASE_URL+"/Reward/Rewardlist"
    let GETNOTIFICATION = BASE_URL+"/Notification/NotificationList"
    let GETMSGLIST = BASE_URL+"/Messages/messagesList"
    
    func getFcm() -> String {
        let token = InstanceID.instanceID().token()
        return token!
    }
    
    func getDeviceId() -> String {
        let id = UIDevice.current.identifierForVendor?.uuidString
        return id!
    }
    
func getdate() -> String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        return result
    }
    
    func loginUser(socialtoken: String, user_type: String, loginType: String, emailORtelephone: String, password: String, deviceid: String,fcmtoken: String,currentdate: String, completion: @escaping (String,Bool)->Void) {
        
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: LOGIN_BASE_URL, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["email_telephone"] = emailORtelephone as AnyObject
            param["user_pass"] = password as AnyObject
            param["fcm_token"] = fcmtoken as AnyObject
            param["login_type"] = loginType as AnyObject
            param["user_type"] = user_type as AnyObject
            param["social_token"] = socialtoken as AnyObject
            param["current_date"] = currentdate as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            param["device_id"] = deviceid as AnyObject
            return param
        },
        onResponse: { (response) in
            if let response = response {
                print("Response",response)
                let message = response["Message"] as! String
                let response_status = response["Status"] as! Bool
                completion(message, response_status)
                }
        })
        {
            (error) in
            
        }
        
    }
        func signupUser(username: String, usertype: String, userpass: String, useremail: String, usermobile: String, useradd: String, completion: @escaping (String,Bool,Register)->Void) {
           
            HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: SIGNUP_BASE_URL, parameters: { () -> [String : AnyObject] in
                var param = [String:AnyObject]()
                param["user_name"] = username as AnyObject
                param["user_pass"] = userpass as AnyObject
                param["fcm_token"] = self.getFcm() as AnyObject
                param["user_email"] = useremail as AnyObject
                param["user_type"] = usertype as AnyObject
                param["user_mobile"] = usermobile as AnyObject
                param["user_add"] = useradd as AnyObject
                param["API_KEY"] = self.API_KEY as AnyObject
                param["device_id"] = self.getDeviceId() as AnyObject
                
                
                return param
            },
            onResponse: { (response) in
                if let response = response {
                    var regdata: Register
                    let message = response["Message"] as! String
                    let response_status = response["Status"] as! Bool
                    if response_status {
                        let data = response["data"] as! Dictionary<String,Any>
                        let reg = data
                        regdata = Register(dict: reg)
                        
                        completion(message,response_status,regdata)
                    }
                }
            })
            {
                (error) in
            }
    }
    

    func chooseLocation(completion: @escaping (String, Bool,[Locations])->Void) {
        var locData = [String]()
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: LOCATION_URL, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
           
            return param
        },
        onResponse: { (response) in
           if let response = response {
           var locList: [Locations] = []
           let message = response["Message"] as! String
           let response_status = response["Status"] as! Bool
           if response_status
            {
                let data = response["data"] as! [Dictionary<String,Any>]
                for locData in data{
                    let reg = locData
                    locList.append(Locations(dict: reg))
                }
                completion(message,response_status,locList)
            }
        }
        })
        {
            (error) in
        }
    }


func chooseCategory(locationId: String, completion: @escaping (String, Bool,[Categories])->Void) {
    var catData = [String]()
    HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: GETCATEGORIES, parameters: { () -> [String : AnyObject] in
        var param = [String:AnyObject]()
        param["API_KEY"] = self.API_KEY as AnyObject
        param["location_id"] = locationId as AnyObject
        return param
    },
    onResponse: { (response) in
    if let response = response {
        var catData: [Categories] = []
        let message = response["Message"] as! String
        let response_status = response["Status"] as! Bool
        if response_status {
            let data = response["data"] as! [Dictionary<String,Any>]
            for getData in data{
                let reg = getData
                catData.append(Categories(dict: reg))
            }
        completion(message,response_status,catData)
        }
     }
    })
    {
        (error) in
    }
}
    
func availableStore(categoryId: String, completion: @escaping (String, Bool,[Stores])->Void) {
        var storeData = [String]()
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: STORELIST, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["category_id"] = categoryId as AnyObject
            return param
        },
        onResponse: { (response) in
        if let response = response {
            var storeData: [Stores] = []
            let message = response["Message"] as! String
            let response_status = response["Status"] as! Bool
            if response_status {
                let data = response["data"] as! [Dictionary<String,Any>]
                for getData in data{
                    let reg = getData
                    storeData.append(Stores(dict: reg))
                    }
            completion(message,response_status,storeData)
            }
            }
        })
        {
            (error) in
        }
    }
func resetPassword(useremail:String, oldpassword:String, newpassword:String, completion: @escaping (String,Bool)->Void){
    HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: RESETPASSWORD, parameters: { () -> [String : AnyObject] in
    var param = [String:AnyObject]()
    param["user_email"] = useremail as AnyObject
    param["old_password"] = oldpassword as AnyObject
    param["new_password"] = newpassword as AnyObject
    param["API_KEY"] = self.API_KEY as AnyObject
    
    return param
    },
    onResponse: { (response) in
    if let response = response{
        print("Response",response)
    
    let message = response["Message"] as! String
    let response_status = response["Status"] as! Bool
    
    completion(message,response_status)
    }
    })
    {
    (error) in
    
    }
    
    }
    
    func editProfile(username: String, useremail: String, usermobile: String, useradd: String, userid: String, completion: @escaping (String,Bool)->Void) {
        
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: UPDATEUSER, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["user_name"] = username as AnyObject
            param["user_email"] = useremail as AnyObject
            param["user_mobile"] = usermobile as AnyObject
            param["user_address"] = useradd as AnyObject
            param["user_id"] = userid as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            return param
        },
        onResponse: { (response) in
        if let response = response {
        let message = response["Message"] as! String
        let response_status = response["Status"] as! Bool
            completion(message, response_status)
            }
        })
        {
            (error) in
        }
   }
    
    func sendFeedback(feedback: String, userid: String, completion: @escaping (String,Bool)->Void) {
        
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: SENDFEEDBACK, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["feedback"] = feedback as AnyObject
            param["user_id"] = userid as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            return param
        },
       onResponse: { (response) in
       if let response = response {
           let message = response["Message"] as! String
           let response_status = response["Status"] as! Bool
           completion(message, response_status)
           }
        })
        {
            (error) in
        }
    }

    func createorder(itemArray : AnyObject, storeId:String, customerId:String,completion:@escaping (String,Bool)->Void){
       
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: ORDERCREATE, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["store_id"] = storeId as AnyObject
            param["current_date"] = self.getdate() as AnyObject
            param["customer_id"] = customerId as AnyObject
            param["itemsArr"] = itemArray as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            
            return param
        },
        onResponse: { (response) in
        if let response = response{
                print("Response",response)
                let message = response["Message"] as! String
                let response_status = response["Status"] as! Bool
                completion(message,response_status)
            }
        })
        {
            (error) in
            
        }
        
    }
    
    func forgotPassword(email:AnyObject, completion:@escaping (String,Bool)->Void){
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: FORGOTPASSWORD, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["user_email"] = email as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            
            return param
        },
        onResponse: { (response) in
        if let response = response{
           print("Response",response)
           let message = response["Message"] as! String
           let response_status = response["Status"] as! Bool
           completion(message,response_status)
           }
        })
        {
            (error) in
        }
    }
    func savelist(listid:String, userid:String, itemArray:AnyObject, listname:String, completion:@escaping (String,Bool)->Void){
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: SAVELIST, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["user_id"] = userid as AnyObject
            param["list_name"] = listname as AnyObject
            param["itemsArr"] = itemArray as AnyObject
            param["list_id"] = listid as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            
            return param
        },onResponse: { (response) in
            if let response = response{
                print("Response",response)
                let message = response["Message"] as! String
                let response_status = response["Status"] as! Bool
                completion(message,response_status)
            }
        })
        {
            (error) in
            
        }
        
    }
    func getListNames(user_id:String,completion:@escaping (String,Bool,[SavedList])->Void)
    {
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: GETLISTNAME, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["user_id"] = user_id as AnyObject
            
            return param
            
        },
        onResponse: { (response) in
        if let response = response{
            print("Response",response)
            var savedList:[SavedList] = []
            let message = response["Message"] as! String
            let response_status = response["Status"] as! Bool
            if response_status {
               let data = response["data"] as! [Dictionary<String,Any>]
               for d in data{
                   let reg = d 
                   savedList.append(SavedList(dict: reg))
                   }
               }
           completion(message,response_status,savedList)
           }
       }) {
        (error) in
        }
    }
    func getitemsFromsavedlist(list_id:String,completion:@escaping (String,Bool,[ItemList])->Void)
    {
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: GETITEMSFROMLIST, parameters: { () -> [String : AnyObject] in
            
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["list_id"] = list_id as AnyObject
            
            return param
            
        },
        onResponse: { (response) in
        if let response = response{
        print("Response",response)
        var saveditemList:[ItemList] = []
        let message = response["Message"] as! String
        let response_status = response["Status"] as! Bool
        if response_status {
            let data = response["data"] as! [Dictionary<String,Any>]
            for d in data{
            let reg = d 
            saveditemList.append(ItemList(dict: reg))
            }
            }
            completion(message,response_status,saveditemList)
            }
        }) {
            (error) in
        }
    }

    func viewOrders(user_id:String,completion:@escaping (String,Bool,[Orders])->Void)
    {
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: ORDERLIST, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["user_id"] = user_id as AnyObject
            
            return param
        },
        onResponse: { (response) in
        if let response = response{
        print("Response",response)
        var savedList:[Orders] = []
        let message = response["Message"] as! String
        let response_status = response["Status"] as! Bool
        if response_status {
           let data = response["data"] as! [Dictionary<String,Any>]
           for d in data{
           let reg = d
           savedList.append(Orders(dict: reg))
           }
        }
        completion(message,response_status,savedList)
        }
        }) {
            (error) in
        }
    }

    func deleteList(listid:String,completion:@escaping (String,Bool)->Void)
    {
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: DELETELIST, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["list_id"] = listid as AnyObject
            param["API_KEY"] = self.API_KEY as AnyObject
            
            return param
        },
        onResponse: { (response) in
        if let response = response{
        print("Response",response)
        let message = response["Message"] as! String
        let response_status = response["Status"] as! Bool
        completion(message,response_status)
        }
        })
        {
            (error) in
        }
    }
    func redeemPoint(userid: String, completion: @escaping (String,String,String,Int)->Void) {

        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: REDEEMPOINTS, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["user_id"] = userid as AnyObject
            return param
        },
        onResponse: { (response) in
            if let response = response{
                let message = response["Message"] as! String
                let response_status = response["Status"] as! Bool
                var earnedpoints = response["order_reward_point"] as! String
                var completeorderpoints = response["total_used_points"] as! String
                var redeemedpoints = response["user_redeemd_point"] as! String
                var totalorder = response["total_order"] as! Int
                completion(earnedpoints,completeorderpoints,redeemedpoints,totalorder)
            }
        })
        {
            (error) in
        }
    }
    func getNotifications(userid: String, completion: @escaping (String, Bool,[Notify])->Void) {
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: GETNOTIFICATION, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["user_id"] = userid as AnyObject
            return param
        },
        onResponse: { (response) in
        if let response = response {
            print(response)
           var getNotif: [Notify] = []
           let message = response["Message"] as! String
           let response_status = response["Status"] as! Bool
               if response_status {
                  let data = response["data"] as! [Dictionary<String,Any>]
                  for getData in data{
                      let reg = getData
                      getNotif.append(Notify(dict: reg))
                      }
                  completion(message,response_status,getNotif)
                  }
               }
        })
        {
            (error) in
        }
    }
    func getMessages(userid: String, completion: @escaping (String, Bool,[Messages])->Void) {
        HitApi.apiLinker.setProgress(isProgress: true).requestMethod(method: .post).execute(url: GETMSGLIST, parameters: { () -> [String : AnyObject] in
            var param = [String:AnyObject]()
            param["API_KEY"] = self.API_KEY as AnyObject
            param["user_id"] = userid as AnyObject
            return param
        },
        onResponse: { (response) in
        if let response = response {
           print(response)
           var getmsg: [Messages] = []
           let message = response["Message"] as! String
           let response_status = response["Status"] as! Bool
           if response_status {
              let data = response["data"] as! [Dictionary<String,Any>]
              for getData in data{
                  let reg = getData
                  getmsg.append(Messages(dict: reg))
                  }
                  completion(message,response_status,getmsg)
                  }
              }
        })
        {
            (error) in
        }
    }
}
    
    



    
    
    
 

