
import UIKit
import SVProgressHUD

class ResetPasswordVC: UIViewController {


    @IBOutlet weak var tfOldPass: UITextField!
    
    @IBOutlet weak var tfNewPass: UITextField!
    
    @IBOutlet weak var tfConfirmPass: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       leftImageTextfield(image: "lock-3", textfield: tfOldPass)
       leftImageTextfield(image: "lock-3", textfield: tfNewPass)
       leftImageTextfield(image: "lock-3", textfield: tfConfirmPass)
    }
    
    func leftImageTextfield(image: String, textfield: UITextField){
        let imageView = UIImageView(image: UIImage(named: image));
        imageView.frame = CGRect(x: 8, y: 4, width: 24, height: 24);
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
    }

    @IBAction func btnResetTapped(_ sender: Any) {
        if self.tfNewPass.text!.isEmpty || self.tfOldPass.text!.isEmpty || self.tfConfirmPass.text!.isEmpty {
                let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter required data", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
        } else if (self.tfNewPass.text != self.tfConfirmPass.text){
            
            let alertController = UIAlertController(title: "Invalid Operation", message: "Please Re-enter the Password ", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        } else{
            SVProgressHUD.show()
            let userEmail = UserDefaults.standard.string(forKey: "email")!
            HitApi.share().resetPassword (useremail: userEmail, oldpassword: self.tfOldPass.text!, newpassword: self.tfNewPass.text!) { (message, status) in
                if status{
                  SVProgressHUD.dismiss()
                  ToastView.shared.short(self.view, txt_msg: message)
                  print(message)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        self.dismiss(animated: true, completion: nil)
                        self.tfNewPass.text = ""
                        self.tfOldPass.text = ""
                        self.tfConfirmPass.text = ""
                    })
                } else {
                    ToastView.shared.short(self.view, txt_msg: message)
                    print(message)
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
