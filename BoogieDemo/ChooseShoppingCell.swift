


import UIKit
protocol TblCell {
    func deleteRow(indx: Int)
    func addRow(indx: Int)
}

class ChooseShoppingCell: UITableViewCell {

    @IBOutlet weak var tfItem: UITextField!
    
    @IBOutlet weak var tfQty: UITextField!
    
    @IBOutlet weak var tfBrand: UITextField!
    @IBOutlet weak var btnAddRow: UIButton!
    
    @IBOutlet weak var btnDeleteRow: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var delegate: TblCell?
    var index: IndexPath?
    
   
    @IBAction func btnRemoveOrder(_ sender: Any) {
        delegate?.deleteRow(indx: (index?.row)!)
    }
    
    @IBAction func btnAddOrder(_ sender: Any) {
        delegate?.addRow(indx: (index?.row)!)
    }
}
