

import UIKit
import DropDown
import SVProgressHUD
struct Locations {
    
    var id: String?
    var locationName: String?
 
    
    init(dict: [String: Any]){
        self.id = dict["id"] as? String
        self.locationName = dict["location_name"] as? String
      
    }
}
    

class ChooseLocation: UIViewController {

    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var btnChooseLocation: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    let dropdownChooseLocation = DropDown()
    var locList = [Locations]()
    var loc_id = String()
    var loc_name = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        btnChooseLocation.layer.cornerRadius = 10
         btnNext.layer.cornerRadius = 10
       let username = UserDefaults.standard.string(forKey: "username")!
        let userid = UserDefaults.standard.string(forKey: "userid")!
        print(userid)
      lblWelcome.text = "Welcome \(username)"
       
        
    }
    func dropDownSubData(subName:[String]){
        dropdownChooseLocation.anchorView = btnChooseLocation
        dropdownChooseLocation.dataSource = subName
        dropdownChooseLocation.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnChooseLocation.setTitle(item, for: .normal)
            self.loc_id = self.locList[index].id!
            self.loc_name = self.locList[index].locationName!
            UserDefaults.standard.set(self.loc_id, forKey: "locid")
            UserDefaults.standard.set(self.loc_name, forKey: "locname")
        }
      
        dropdownChooseLocation.bottomOffset = CGPoint(x: 0, y: 30)
        dropdownChooseLocation.textFont = UIFont.boldSystemFont(ofSize: 18)
    }
  
    
    @IBAction func btnChooseLocHit(_ sender: Any) {
        SVProgressHUD.show()
        HitApi.share().chooseLocation() { (message, status, response) in
            
            if response.count > 0 {
                self.locList = response
                self.dropDownSubData(subName: self.locList.map{$0.locationName!})
                self.dropdownChooseLocation.show()
            } else {
                    let alertController = UIAlertController(title: "Error", message: "No Location Available!", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
            }
    
        }
    }

    @IBAction func btnNextHit(_ sender: Any) {
        if (self.btnChooseLocation.currentTitle == "Choose Location") {
            let alertController = UIAlertController(title: "Empty Field", message: "Please choose a location!", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
       
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let data = storyboard.instantiateViewController(withIdentifier: "TabBarClass") as! TabBarClass
            let subdata = data.viewControllers![0] as! ChooseServiceTabVC
            subdata.locId = self.loc_id
            subdata.locName = self.loc_name
            navigationController?.pushViewController(data, animated: true)

        }
}
}
