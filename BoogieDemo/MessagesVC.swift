

import UIKit
import WebKit

class Messages{
    let msgId: String?
    let msgDesc: String?
    var createdOn: String?
    var status: String?
    
    init(dict: [String: Any]){
        self.msgId = dict["id"] as? String
        self.msgDesc = dict["description"] as? String
        self.createdOn = dict["created_on"] as? String
        self.status = dict["status"] as? String
    }
}

class MessagesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
      @IBOutlet weak var tblMessages: UITableView!
    
    var msgs = [Messages]()
      override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMessages.delegate = self
        self.tblMessages.dataSource = self
        getMessages()
    }
    func getMessages(){
        let userid = "11" //UserDefaults.standard.string(forKey: "userid")!
        HitApi.share().getMessages(userid: userid) { (message, status, response) in
            if status{
                self.msgs = response
                DispatchQueue.main.async {
                    self.tblMessages.reloadData()
            }
        

            }
            else{
                ToastView.shared.short(self.view, txt_msg: message)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.msgs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesCell", for: indexPath) as! MessagesCell
        cell.lblDateTime.text = msgs[indexPath.row].createdOn
        let data  = msgs[indexPath.row].msgDesc!
        print(data)
        cell.wbMsgView.loadHTMLString(data, baseURL: nil)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}
