

import UIKit
import FBSDKLoginKit
import SVProgressHUD

class LoginVC: UIViewController, FBSDKLoginButtonDelegate {
   
    

    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfEmailAdress: UITextField!
  
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftImageTextfield(image: "close-envelope-2", textfield: tfEmailAdress)
         leftImageTextfield(image: "lock-3", textfield: tfPassword)
     
      addshodow(component: btnLogin)
        
    let btnFBLogin = FBSDKLoginButton()
       
        btnFBLogin.center.x = self.view.center.x
        btnFBLogin.frame = CGRect(x: (self.view.center.x - btnFBLogin.frame.width/2), y: self.view.frame.height - 134, width: btnFBLogin.frame.width, height: btnFBLogin.frame.height)
        btnFBLogin.delegate = self
        btnFBLogin.readPermissions = ["public_profile", "email"]
        
        
       self.view.addSubview(btnFBLogin)
        
    
        if FBSDKAccessToken.current() != nil {
            print("Already Logged in")
            
        }else{
            print("not logged in")
        }
        
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error.localizedDescription)
        }else if result.isCancelled {
            print("user cancelled")
        }else {
            print("Logged in")
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("user logout")
    }
     
    
    
    func addshodow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
        component.layer.borderWidth = 1
        component.layer.borderColor = UIColor.white.cgColor
        component.layer.cornerRadius = 10
    }
 

    @IBAction func btnSignupClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        navigationController?.pushViewController(data, animated: true)
    }
    func leftImageTextfield(image: String, textfield: UITextField){
        let imageView = UIImageView(image: UIImage(named: image));
        imageView.frame = CGRect(x: 8, y: 4, width: 24, height: 24);
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
    }
    @IBAction func btnLoginTapped(_ sender: Any) {
        if self.tfEmailAdress.text!.isEmpty || self.tfPassword.text!.isEmpty {
            
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter  required data", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else if !isValidEmail(self.tfEmailAdress.text!) {
            let alertController = UIAlertController(title: "Invalid Format", message: "Please enter valid username", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            SVProgressHUD.show()
           
            let deviceid = UserDefaults.standard.string(forKey: "deviceid")!
            let fcmtoken = UserDefaults.standard.string(forKey: "fcmtoken")!
            let logintype = UserDefaults.standard.string(forKey: "logintype")!
            let currentdate = UserDefaults.standard.string(forKey: "currentdate")!
            
            HitApi.share().loginUser(socialtoken: "", user_type: "customer", loginType: logintype, emailORtelephone: self.tfEmailAdress.text!, password: self.tfPassword.text!, deviceid: deviceid,fcmtoken: fcmtoken, currentdate: currentdate) { (message, status) in
                if status{
                    print("Login Successfull")
                    
                    UserDefaults.standard.set(self.tfEmailAdress.text, forKey: "emailOrTel")
                    UserDefaults.standard.set(self.tfPassword.text, forKey: "password")
                    SVProgressHUD.dismiss()
                    
                    ToastView.shared.short(self.view, txt_msg: "User Logged in successfully")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let data = storyboard.instantiateViewController(withIdentifier: "ChooseLocation") as! ChooseLocation
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        self.navigationController?.pushViewController(data, animated: true)
                    })
                    
                    self.tfEmailAdress.text! = ""
                    self.tfPassword.text! = ""
                }
            
            else{
                    
                    SVProgressHUD.dismiss()
                    let alertController = UIAlertController(title: "Invalid Login", message: "Please enter valid username or password", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
        }
        }
    
    func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    @IBAction func btnForgotpass(_ sender: Any) {
        if (self.tfEmailAdress.text?.isEmpty)! {
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter your Email", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            SVProgressHUD.show()
            let user_id = UserDefaults.standard.string(forKey: "userid")!
            HitApi.share().forgotPassword(email: self.tfEmailAdress.text! as AnyObject) { (message, status) in
                if status{
                    SVProgressHUD.dismiss()
                    ToastView.shared.short(self.view, txt_msg: message)
                    print(message)
                } else {
                    ToastView.shared.short(self.view, txt_msg: message)
                    print(message)
                }
            }
        }
      }
}
