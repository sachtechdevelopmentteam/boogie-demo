

import UIKit

class ProfileTabVC: UIViewController {

    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var vwEditprofile: UIView!
    @IBOutlet weak var vwShoppimgList: UIView!
    @IBOutlet weak var vwRewardPoint: UIView!
    @IBOutlet weak var vwFeedback: UIView!
    @IBOutlet weak var vwSettings: UIView!
    @IBOutlet weak var btnLogout: UIButton!
    
    @IBOutlet weak var vwMessages: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let username = UserDefaults.standard.string(forKey: "username")!
       lblWelcome.text = "Welcome \(username)"
      addshodow(component: vwEditprofile)
      addshodow(component: vwShoppimgList)
      addshodow(component: vwRewardPoint)
      addshodow(component: vwFeedback)
      addshodow(component: vwSettings)
        addshodow(component: vwSettings)
      addshodow(component: vwMessages)
    }
    
    func addshodow(component:UIView)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
        component.layer.cornerRadius = 10
    }
    func addshodow(component: UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
        component.layer.cornerRadius = 10
    }
   
    @IBAction func btnEditProfileTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.present(data, animated: true)
    }
    
    @IBAction func btnMessagesTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "MessagesVC") as! MessagesVC
        self.present(data, animated: true)
    
    }
    
    @IBAction func btnSettingTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        
        self.present(data, animated: true)
    }
    @IBAction func btnFeedbackTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        self.present(data, animated: true)
    }
    @IBAction func btnShoppingListTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "ShoppingListVC") as! ShoppingListVC
        self.present(data, animated: true)
    }
    @IBAction func btnLogoutHit(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func btnRewardTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "RedeemVC") as! RedeemVC
        self.present(data, animated: true)
    }
    
}
