

import UIKit

class ViewOrderCell: UITableViewCell {

    @IBOutlet weak var lblStoreNameCell: UILabel!
    
    @IBOutlet weak var lblDateCell: UILabel!
    
    @IBOutlet weak var lblStatusCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblStoreNameCell.layer.borderWidth = 2
        lblStoreNameCell.layer.borderColor = UIColor.white.cgColor
        lblDateCell.layer.borderWidth = 2
        lblDateCell.layer.borderColor = UIColor.white.cgColor
        lblStatusCell.layer.borderWidth = 2
        lblStatusCell.layer.borderColor = UIColor.white.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
