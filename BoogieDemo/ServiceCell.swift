

import UIKit

class ServiceCell: UICollectionViewCell {
    
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var imgService: UIImageView!
    var category_id = String()
}
