


import UIKit

class SettingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnTermsHit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
    
        self.present(data, animated: true)
    }
    @IBAction func btnPrivacyHit(_ sender: Any) {
    }
    @IBAction func btnNotificationHit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.present(data, animated: true)
    }
}
