
import UIKit
import SVProgressHUD

class EditProfileVC: UIViewController {

    @IBOutlet weak var tfEditName: UITextField!
    
    @IBOutlet weak var tfEditAddress: UITextField!
    
    @IBOutlet weak var tfEditEmail: UITextField!
    @IBOutlet weak var tfEditMobile: UITextField!
    var userid = String()
    override func viewDidLoad() {
        super.viewDidLoad()
     tfEditName.text = UserDefaults.standard.string(forKey: "username")!
     tfEditEmail.text = UserDefaults.standard.string(forKey: "email")!
     tfEditAddress.text = UserDefaults.standard.string(forKey: "address")!
     tfEditMobile.text = UserDefaults.standard.string(forKey: "Mobile")!
     self.userid = UserDefaults.standard.string(forKey: "userid")!
    }
    
    @IBAction func btnSaveDetailsTapped(_ sender: Any) {
        SVProgressHUD.show()
        HitApi.share().editProfile(username: tfEditName.text!, useremail: tfEditEmail.text!, usermobile: tfEditMobile.text!, useradd: tfEditAddress.text!, userid: userid) { (message, status) in
            if status{
                print(message)
                
                UserDefaults.standard.set(self.tfEditName.text, forKey: "username")
                UserDefaults.standard.set(self.tfEditEmail.text, forKey: "email")
                UserDefaults.standard.set(self.tfEditAddress.text, forKey: "address")
                UserDefaults.standard.set(self.tfEditMobile.text, forKey: "Mobile")
                SVProgressHUD.dismiss()
                
                ToastView.shared.short(self.view, txt_msg: message)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                    self.dismiss(animated: true, completion: nil)
                    self.tfEditName.text! = ""
                    self.tfEditAddress.text! = ""
                    self.tfEditEmail.text! = ""
                    self.tfEditMobile.text! = ""
                })
                
              
            }
            else{
                SVProgressHUD.dismiss()
              ToastView.shared.short(self.view, txt_msg: message)
                
            }
        }
       
    }
    

    @IBAction func btnResetPassTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        self.present(data, animated: true)
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
