
import UIKit

class ShoppingListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

    @IBOutlet weak var lblHeadList: UILabel!
    @IBOutlet weak var lblHeadDelete: UILabel!
    @IBOutlet weak var lblHeadDate: UILabel!
    
    @IBOutlet weak var tblShoppingListTable: UITableView!
    var listInfo = [SavedList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblShoppingListTable.delegate = self
        self.tblShoppingListTable.dataSource = self
        
        lblHeadList.layer.borderWidth = 2
        lblHeadList.layer.borderColor = UIColor.white.cgColor
        lblHeadDelete.layer.borderWidth = 2
        lblHeadDelete.layer.borderColor = UIColor.white.cgColor
        lblHeadDate.layer.borderWidth = 2
        lblHeadDate.layer.borderColor = UIColor.white.cgColor
        getLists()
    }
    func getLists(){
        let userid = UserDefaults.standard.string(forKey: "userid")!
        print(userid)
        HitApi.share().getListNames(user_id: userid) { (message, status, response) in
            
            if response.count > 0 {
                self.listInfo = response
                DispatchQueue.main.async {
                    self.tblShoppingListTable.reloadData()
                }
            }
            else {
                ToastView.shared.short(self.view, txt_msg: "No list is available.")
            }
            
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingListCell", for: indexPath) as! ShoppingListCell
        cell.lblCellListName.text = listInfo[indexPath.row].listname
        cell.lblCellDate.text = listInfo[indexPath.row].currentdate
        cell.btnDeleteCell.tag = indexPath.row
        cell.btnDeleteCell.addTarget(self, action: #selector(deleteList), for: .touchUpInside)
        return cell
    }

    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func deleteList(sender: UIButton){
        let index = sender.tag
        print(index)
        let list_id = listInfo[index].listid!
        HitApi.share().deleteList(listid: list_id) { (message, status) in
            
            if status {
                if self.listInfo.count > 1{
                    self.listInfo.remove(at: index)
                    self.tblShoppingListTable.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                    self.tblShoppingListTable.reloadData()
                    DispatchQueue.main.async {
                    self.tblShoppingListTable.reloadData()
                    }
                ToastView.shared.short(self.view, txt_msg: message)
                }}
            else {
                ToastView.shared.short(self.view, txt_msg: message)
                }
            }
            
        }
}
