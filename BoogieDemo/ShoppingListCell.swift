
import UIKit

class ShoppingListCell: UITableViewCell {

    @IBOutlet weak var lblCellListName: UILabel!
    
    @IBOutlet weak var lblCellDate: UILabel!
    
    @IBOutlet weak var btnDeleteCell: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblCellListName.layer.borderWidth = 2
        lblCellListName.layer.borderColor = UIColor.white.cgColor
        lblCellDate.layer.borderWidth = 2
        lblCellDate.layer.borderColor = UIColor.white.cgColor
        btnDeleteCell.layer.borderWidth = 2
        btnDeleteCell.layer.borderColor = UIColor.white.cgColor
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     
    }

}
