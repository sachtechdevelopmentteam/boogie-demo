

import UIKit
class Orders {
    var storeName: String?
    var createdDate: String?
    var orderStatus: String?
    
    init(dict: [String: Any]){
        self.storeName = dict["store_name"] as? String
        self.orderStatus = dict["order_status"] as? String
        self.createdDate = dict["created_on"] as? String
    }
}

class ViewOrderVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblStore: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var tblViewOrderTable: UITableView!
    
    var orderList = [Orders]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblViewOrderTable.delegate = self
        self.tblViewOrderTable.dataSource = self
        let username = UserDefaults.standard.string(forKey: "username")!
        lblWelcome.text = "Welcome \(username)"
     
        lblStore.layer.borderWidth = 2
        lblStore.layer.borderColor = UIColor.white.cgColor
        lblDate.layer.borderWidth = 2
        lblDate.layer.borderColor = UIColor.white.cgColor
        lblStatus.layer.borderWidth = 2
        lblStatus.layer.borderColor = UIColor.white.cgColor
        getOrders()
    }
    func getOrders(){
        let userid = UserDefaults.standard.string(forKey: "userid")!
        print(userid)
        HitApi.share().viewOrders(user_id: userid) { (message, status, response) in
            
            if response.count > 0 {
                self.orderList = response
                DispatchQueue.main.async {
                    self.tblViewOrderTable.reloadData()
                }
            }
            else {
                ToastView.shared.short(self.view, txt_msg: "No orders is available.")
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewOrderCell", for: indexPath) as! ViewOrderCell
        cell.lblStoreNameCell.text = orderList[indexPath.row].storeName
        cell.lblStatusCell.text = orderList[indexPath.row].orderStatus
        cell.lblDateCell.text = orderList[indexPath.row].createdDate
        return cell
    }

}
